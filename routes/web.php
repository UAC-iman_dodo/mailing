<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(array('middleware'=>'AuthSso'), function(){
	Route::get('/', 'Emails@inbox');


	Route::get('/inbox', 'Emails@inbox')->name('inbox');
	Route::get('/outbox', 'Emails@outbox')->name('outbox');
	Route::get('/draft', 'Emails@draft')->name('draft');

	Route::get('/add', 'Emails@add')->name('add');
	Route::post('/send', 'Emails@send')->name('send');

	Route::get('/edit/{id}', 'Emails@edit')->name('edit');
	Route::post('/update/{id}', 'Emails@update')->name('update');
	Route::post('/delete/{id}', 'Emails@delete')->name('delete');

	Route::get('/detail/{id}', 'Emails@show_email')->name('detail');

	Route::get('/get', 'Emails@get_email')->name('get');
});