<?php

return [
    'host_url' => env('UA_HOST_URL', 'http://localhost:8000/server'),
    'broker_id' => env('UA_BROKER_ID', 3214421),
    'broker_secret' => env('UA_BROKER_SECRET', '885c5d84f73e89989f42bc8631da30428d6310d5'),
    'uri' => [
        'profile' => 'profile',
        'login' => 'login',
        'logout' => 'logout',
    ]
];