@extends('tempt')

@section('content')

<div class="content">
    
	<div class="container-fluid">
			<div class="row">
				<div class="col-xl-12">
						<div class="breadcrumb-holder">
                                <h1 class="main-title float-left">{{$title}}</h1>
                                <ol class="breadcrumb float-right">
									<li class="breadcrumb-item active">{{$subtitle}}</li>
                                </ol>
                                <div class="clearfix"></div>
                        </div>
				</div>
			</div>

			<div class="row">
			
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">						
					<div class="card mb-3">
						<div class="card-header">
						</div>
							
						<div class="card-body">
							<div class="table-responsive">
							<table id="example1" class="table table-bordered table-hover display">
								<thead>
									<tr>
										<th>Subject</th>
										<th>{{ ($subtitle=='Inbox'?'From':'To') }}</th>
										<th>Messages</th>
										<th>Date</th>
										<th></th>
									</tr>
								</thead>							
								<tbody>
									@foreach ($email as $mail)
										<tr>
											@if($subtitle=='Inbox')
												<td>{{ @$mail->emails->subject }}</td>
												<td></td>
												<td>{{ @$mail->emails->messages }}</td>
											@else
												<td>{{ $mail->subject }}</td>
												<td></td>
												<td>{{ $mail->messages }}</td>
												@foreach(@$mail->targets as $target)
												{{$target->emails}}<br>
												@endforeach
											@endif
											<td>{{ $mail->created_at }}</td>
											<td>
												<a href="{{ route(($subtitle=='Draft'?'edit':'detail'), ['id'=>$mail->id_email]) }}" class="btn btn-primary">Detail</a>
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
							</div>
							
						</div>														
					</div><!-- end card-->					
				</div>

			</div>

    </div>
	<!-- END container-fluid -->

</div>
@endsection