@extends('tempt')

@section('content')

<div class="content">
    
	<div class="container-fluid">

			<div class="row">
				<div class="col-xl-12">
						<div class="breadcrumb-holder">
                                <h1 class="main-title float-left">Email</h1>
                                <ol class="breadcrumb float-right">
									<li class="breadcrumb-item active">Detail Email</li>
                                </ol>
                                <div class="clearfix"></div>
                        </div>
				</div>
			</div>
			<div class="row">			
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">						
					<div class="card mb-3">
						<div class="card-header">
						</div>
							
						<div class="card-body">
							
							<div class="card">
							  <div class="card-body">
								<h4 class="card-title">{{ @$email->subject }}</h4>
								<h6 class="card-subtitle mb-2 text-muted">
										@foreach($email->targets as $target)
										{{@$target->emails}}<br>
										@endforeach
								</h6>
								<p class="card-text">{{ $email->messages }}</p>
								<p class="card-text">{{ $email->created_at }}</p>
							  </div>
							</div>
						</div>							
					</div><!-- end card-->					
                </div>
			</div>

    </div>
	<!-- END container-fluid -->

</div>
@endsection