
<div class="left main-sidebar">

	<div class="sidebar-inner leftscroll">

		<div id="sidebar-menu">
    
			<ul>

				<li class="submenu">
					<a href="{{ route('inbox') }}"><i class="fa fa-envelope bigfonts"></i><span> Inbox </span> </a>
                </li>

				<li class="submenu">
                    <a href="{{ route('outbox') }}"><i class="fa fa-envelope-o bigfonts"></i><span> Outbox </span> </a>
                </li>

                <li class="submenu">
                    <a href="{{ route('draft') }}"><i class="fa fa-envelope-square bigfonts"></i><span> Draft </span> </a>
                </li>
                <li class="submenu">
                    <a href="{{ route('add') }}"><i class="fa fa-envelope-opens bigfonts"></i><span> Send new email </span> </a>
                </li>
					
            </ul>

            <div class="clearfix"></div>

		</div>
    
		<div class="clearfix"></div>

	</div>

</div>