@extends('tempt')

@section('content')

<div class="content">
    
	<div class="container-fluid">

			<div class="row">
				<div class="col-xl-12">
						<div class="breadcrumb-holder">
                                <h1 class="main-title float-left">Email</h1>
                                <ol class="breadcrumb float-right">
									<li class="breadcrumb-item active">{{$subtitle}}</li>
                                </ol>
                                <div class="clearfix"></div>
                        </div>
				</div>
			</div>
			<div class="row">			
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">						
					<div class="card mb-3">
						<div class="card-header">
						</div>
						<form action="{{$action}}" method="post">
						{{ csrf_field() }}
						<div class="card-body">
							<div class="form-group">
								<label for="example3">Email</label>								
								<select class="form-control select2" id="example3" name="emails[]" multiple="multiple">
									<!-- foreach data user -->
									@foreach($target as $to)
									<option value="{{ json_encode(array('id'=>$to->id,'email'=>$to->email)) }}">{{ $to->email }}</option>
									@endforeach()
									<!-- end foreach -->
								</select>
							</div>
							<label for="Subject">Subject</label>
							<input class="form-control" id="Subject" name="subject" placeholder="Subject" required="" type="text" value="{{ @$email->subject }}">
						</div>
						<div class="card-body">
							<label for="content">Message</label>
							<textarea rows="3" class="form-control" name="content">{{ @$email->messages }}</textarea>
							<!--  add class editor -->
						</div>
						<div class="card-body">
						  	<button type="submit" name="submit" value="0" class="btn btn-primary">Submit</button>
						  	<button type="submit" name="submit" value="1" class="btn btn-primary">Simpan</button>
						</div>
						</form>									
					</div><!-- end card-->					
                </div>
			</div>
    </div>
	<!-- END container-fluid -->

</div>
@endsection
