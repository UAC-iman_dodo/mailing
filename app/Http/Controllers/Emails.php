<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Jasny\SSO\Server;
use App\Models\Email;
use App\Models\Target;
use DB;


	/* TODO :
		- load all inbox data
			- if type = inbox
				- load from target where to_id = id logged in
					- join with email
				- where status = sended
			- else type = draft
				- load from email where from_id = id logged in
					-join with target
				- where status = not sended
			- else type = outbox
				- load from email where from_id = id logged in
					- join with target
				- where status = sended
	*/

class Emails extends Controller
{
    public function inbox(){
        $email = Target::with(['mail'=>function($query){
                        $query->where('status','=', '0');
                    }])->where('to_id','=', request()->auth_user['id'])
                    ->get();
        $title = "Email";
        $subtitle = "Inbox";
        dd($email);
    	return view('views',compact('email','type','title','subtitle'));
    }

    public function outbox(){
    	$email = Email::with('targets')
                    ->where('emails.status','=', '0')
                    ->where('emails.from_id','=', request()->auth_user['id'])
                    ->get();
        $title = "Email";
        $subtitle = "Outbox";
    	return view('views',compact('email','type','title','subtitle'));
    }

    public function draft(){
    	$email = Email::with('targets')
                    ->where('emails.status','=', '1')
                    ->where('emails.from_id','=', request()->auth_user['id'])
                    ->get();
        $title = "Email";
        $subtitle = "Draft";
    	return view('views',compact('email','type','title','subtitle'));
    }

    public function show_email($id){
		$email = Email::with('targets')
					->find($id);
        return view('detail',compact('email'));
    }

    public function add(){
        $user = request()->auth_user;
        $target = json_decode($this->get_email('http://localhost:8000/api/email/'.$user['id']))->data;
        $action = route('send');
        $subtitle = "new Email";
        return view('crud', compact('subtitle', 'action', 'target'));
    }

    // static function
    public function send(Request $request){
        $user = request()->auth_user;
        DB::beginTransaction();
        try {  
            $email = new Email; 
        
            $email->from_id = $user['id'];// change to ids
            $email->subject = $request->input('subject');
            $email->messages = $request->input('content');
            $email->status = $request->input('submit');

            $email->save();

            // TODO : request id_member with email for to_id
            foreach ($request->input('emails') as $row) {
                $rows = json_decode($row);
                $target = new Target;
                $target->email_id = $email->id_email;
                $target->to_id = $rows->id;
                $target->emails = $rows->email;
                $target->onread = 0;

                $target->save();
            } 

            DB::commit();
        }catch (\Exception $e) {
            DB::rollback();
            return $e->getMessage();
        }
        return redirect()->route('inbox');
    }

    public function edit($id){
        $user = request()->auth_user;
        $target = json_decode($this->get_email('http://localhost:8000/api/email/'.$user['id']))->data;
        $email = Email::with('targets')
                    ->find($id);
        $action = route('update', ['id'=>$id]);
        $subtitle = "Edit Email";
        return view('crud',compact('email','target', 'subtitle', 'action'));

    }
	// static function
    public function update(Request $request, $id){
        DB::beginTransaction();
        try {  
        	$email = Email::find($id);

            $email->subject = $request->input('subject');
            $email->messages = $request->input('content');
            $email->status = $request->input('submit');

        	$email->save();

            $targets = Target::where('emails','=',$email->email_id)->orderBy('emails')->get();

            $emails = $request->input('email');
            $i =0;
            foreach ($targets as $target) {
                $rows = Target::find($target->id_target);
                if(in_array($target->emails,$emails)){
                    $rows->to_id = "";
                    $rows->emails = $emails[$i];
                    $rows->status = 0;

                    $rows->save();
                }else{
                    $rows->softDeletes();
                }
                $i++;
            }

            DB::commit();
        }catch (\Exception $e) {
            DB::rollback();
            return $e->getMessage();
        }
        return redirect()->route('draft');
    }

    // static function
    public function delete($id){
    	/* TODO :
    		- update status email (read or not)
    	*/

        $email = Email::find($id);
        $email->softDeletes();

        return back()->withInput();
    }

    public function get_email($url){

        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec ($ch);
        $info = curl_getinfo($ch);
        $http_result = $info ['http_code'];
        curl_close ($ch);

        return $output;
    }
}
