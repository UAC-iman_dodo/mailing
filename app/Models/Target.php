<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Target extends Model
{
    
    protected $table = 'targets';
    protected $primaryKey = 'id_target';

    public function emails(){
    	return $this->belongsTo('App\Models\Email', 'email_id', 'id_email');
    }

    public function mail(){
    	return $this->hasOne('App\Models\Email', 'id_email', 'email_id');
    }
}
