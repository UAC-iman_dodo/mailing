<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    
    protected $table = 'emails';
    protected $primaryKey = 'id_email';

    public function targets(){
    	return $this->hasMany('App\Models\Target', 'email_id', 'id_email');
    }
}
