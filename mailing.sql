-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 05, 2018 at 02:11 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mailing`
--

-- --------------------------------------------------------

--
-- Table structure for table `emails`
--

CREATE TABLE `emails` (
  `id_email` int(10) UNSIGNED NOT NULL,
  `from_id` int(11) NOT NULL,
  `subject` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `messages` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `emails`
--

INSERT INTO `emails` (`id_email`, `from_id`, `subject`, `messages`, `status`, `created_at`, `updated_at`) VALUES
(2, 0, 'fafsa', 'testes', 0, '2018-05-29 02:52:09', '2018-05-29 03:04:34'),
(5, 0, 'fsafa', 'fasfafsa', 0, '2018-05-30 07:02:22', '2018-05-30 07:02:22'),
(6, 0, 'testetsteststs', 'testestes', 0, '2018-05-30 07:03:48', '2018-05-30 07:03:48'),
(9, 0, 'fsafsaffsafsafsa', 'afsafas', 0, '2018-05-30 07:05:59', '2018-05-30 07:05:59'),
(10, 0, 'test draft', 'test', 1, '2018-05-30 07:19:06', '2018-05-30 07:19:06'),
(11, 0, 'test draft', 'test', 1, '2018-05-30 07:19:46', '2018-05-30 07:19:46'),
(12, 0, 'test draft', 'test', 0, '2018-05-30 07:20:56', '2018-05-30 07:20:56'),
(13, 1, 'test', 'testes', 0, '2018-05-30 07:28:13', '2018-05-30 07:28:13'),
(14, 1, 'test drafttest', 'test', 1, '2018-05-30 07:32:36', '2018-05-30 07:32:36'),
(15, 1, 'test send email', 'sending email testing', 0, '2018-06-04 23:22:35', '2018-06-04 23:22:35');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(11, '2018_05_24_134942_create_emails_table', 1),
(12, '2018_05_24_134948_create_targets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `targets`
--

CREATE TABLE `targets` (
  `id_target` int(10) UNSIGNED NOT NULL,
  `email_id` int(11) NOT NULL,
  `to_id` int(11) NOT NULL,
  `emails` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `onread` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `targets`
--

INSERT INTO `targets` (`id_target`, `email_id`, `to_id`, `emails`, `onread`, `created_at`, `updated_at`) VALUES
(1, 2, 0, '0', 0, '2018-05-29 02:52:09', '2018-05-29 02:52:09'),
(2, 1, 2, 'test1@gmail.com', 0, '2018-05-30 07:02:22', '2018-05-30 07:02:22'),
(3, 1, 2, 'test1@gmail.com', 0, '2018-05-30 07:03:48', '2018-05-30 07:03:48'),
(4, 1, 3, 'test2@gmail.com', 0, '2018-05-30 07:03:48', '2018-05-30 07:03:48'),
(5, 9, 2, 'test1@gmail.com', 0, '2018-05-30 07:05:59', '2018-05-30 07:05:59'),
(6, 9, 3, 'test2@gmail.com', 0, '2018-05-30 07:05:59', '2018-05-30 07:05:59'),
(7, 10, 2, 'test1@gmail.com', 0, '2018-05-30 07:19:06', '2018-05-30 07:19:06'),
(8, 11, 2, 'test1@gmail.com', 0, '2018-05-30 07:19:46', '2018-05-30 07:19:46'),
(9, 12, 2, 'test1@gmail.com', 0, '2018-05-30 07:20:56', '2018-05-30 07:20:56'),
(10, 13, 2, 'test1@gmail.com', 0, '2018-05-30 07:28:13', '2018-05-30 07:28:13'),
(11, 14, 3, 'test2@gmail.com', 0, '2018-05-30 07:32:36', '2018-05-30 07:32:36'),
(12, 15, 3, 'test2@gmail.com', 0, '2018-06-04 23:22:35', '2018-06-04 23:22:35');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `emails`
--
ALTER TABLE `emails`
  ADD PRIMARY KEY (`id_email`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `targets`
--
ALTER TABLE `targets`
  ADD PRIMARY KEY (`id_target`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `emails`
--
ALTER TABLE `emails`
  MODIFY `id_email` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `targets`
--
ALTER TABLE `targets`
  MODIFY `id_target` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
